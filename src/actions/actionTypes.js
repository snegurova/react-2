
export const FETCH_DATA = "FETCH_DATA";
export const SET_CHAT_DATA = "SET_CHAT_DATA";
export const SET_PRELOADER = "SET_PRELOADER";
export const ADD_MESSAGE = "ADD_MESSAGE";
export const DELETE_MESSAGE = "DELETE_MESSAGE";