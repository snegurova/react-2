import {FETCH_DATA, SET_CHAT_DATA, SET_PRELOADER, ADD_MESSAGE, DELETE_MESSAGE} from "../actions/actionTypes";

export const fetchData = messages => ({
  type: FETCH_DATA,
  payload: {
    messages
  }
});

export const setChatData = messages => ({
  type: SET_CHAT_DATA,
  payload: {
    messages
  }
});

export const setPreloader = preloader => ({
  type: SET_PRELOADER,
  payload: {
    preloader
  }
});

export const addMessage = messageData => ({
  type: ADD_MESSAGE,
  payload: {
    messageData
  }
});

export const deleteMessage = messageId => ({
  type: DELETE_MESSAGE,
  payload: {
    messageId
  }
});