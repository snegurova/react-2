import React from "react";
import Header from "../Header/Header";
import './Chat.css'
import MessageList from "../MessageList/MessageList";
import Input from "../Input/Input";
import Preloader from "../Preloader/Preloader";
import {connect} from "react-redux";
import {fetchData, setChatData, setPreloader} from "../../actions/actions";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.handleMessageEdit = this.handleMessageEdit.bind(this);
  }
  
  
  async componentDidMount() {
    this.props.setPreloader(true);
    const response = await fetch(this.props.url);
    const messages = await response.json();
    const messagesSorted = messages.sort((a, b) => Date.parse(a['createdAt']) - Date.parse(b['createdAt']));
    this.props.fetchData(messagesSorted);
    this.props.setChatData(messagesSorted);
    this.props.setPreloader(false);
  }
  
  handleMessageEdit(id, value) {
    const messages = this.state.messages.map(it => {
      if (it.id === id) {
        it.text = value;
        it.editedAt = new Date(Date.now());
        return it;
      }
      return it;
    });
    this.setState({ messages });
  }
  
  render() {
    if (this.props.preloader) {
      return <Preloader />;
      
    }
    return (
      <div className="chat-wrapper">
        <Header />
        <MessageList
          onMessageEdit = { this.handleMessageEdit }
        />
        <Input />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    messages: state.chat.chat.messages,
    preloader: state.chat.chat.preloader
  }
};

const mapDispatchToProps = {
  fetchData,
  setChatData,
  setPreloader
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);