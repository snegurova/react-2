import React from "react";
import './Header.css'
import {connect} from "react-redux";

class Header extends React.Component {
  render() {
    return (
      <header className="header">
        <div className="header-title">{this.props.chatTitle}</div>
        <div>
          <span className="header-users-count">{this.props.usersCount}</span>
          <span> participants</span>
        </div>
        <div>
          <span className="header-messages-count">{this.props.messagesCount}</span>
          <span> messages</span>
        </div>
        <div>
          <span>Last message at </span>
          <span className="header-last-message-date">{this.props.lastMessageDate}</span>
        </div>
        
      </header>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chatTitle: state.chat.chat.chatTitle,
    usersCount: state.chat.chat.usersCount,
    messagesCount: state.chat.chat.messagesCount,
    lastMessageDate: state.chat.chat.lastMessageDate
  }
};

export default connect(mapStateToProps)(Header);