import React from "react";
import './Input.css'
import {addMessage, setChatData} from "../../actions/actions";
import {connect} from "react-redux";
import {v4 as uuidv4} from "uuid";

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messageText: ''
    }
    this.handleMessageAdd = this.handleMessageAdd.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  componentDidUpdate(prevProps) {
    this.props.setChatData(this.props.messages);
  }
  onChange({target}) {
    this.setState({ messageText: target.value });
  }
  handleMessageAdd() {
    const date = new Date(Date.now());
    const message = {
      id: uuidv4(),
      userId: this.props.currentUserId,
      avatar: this.props.currentUserAvatar,
      user: this.props.currentUser,
      text: this.state.messageText,
      createdAt: date,
      editedAt: ""
    }
    this.props.addMessage(message);
    
    this.setState({ messageText: '' });
  }
  render() {
    return (
      <div className="message-input">
        <input className="message-input-text" type="text" value={this.state.messageText} onChange={this.onChange}/>
        <button className="message-input-button" onClick={ this.handleMessageAdd }>Send</button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.chat.messages,
    currentUserId: state.chat.chat.currentUserId,
    currentUser: state.chat.chat.currentUser,
    currentUserAvatar: state.chat.chat.currentUserAvatar,
  }
};

const mapDispatchToProps = {
  addMessage,
  setChatData
};

export default connect(mapStateToProps, mapDispatchToProps)(Input);