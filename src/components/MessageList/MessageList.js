import React from "react";
import './MessageList.css'
import Message from "../messages/Message";
import OwnMessage from "../messages/OwnMessage";
import {connect} from "react-redux";

class MessageList extends React.Component {
  render() {
    const massagesListItems = this.props.messages.map(it => {
      const date = new Date(it.createdAt);
      if (it.userId === this.props.currentUserId) {
        return <React.Fragment key = {it.id}>
        <OwnMessage
          messageId = {it.id}
          userName ={it.user}
          messageTime ={`${date.getHours()}:${date.getMinutes()}`}
          messageText ={it.text}
          onMessageEdit = { this.props.onMessageEdit }
        />
        </React.Fragment>;
      } else {
        return <React.Fragment key = {it.id}>
        <Message
          avatarUrl = {it.avatar}
          userName ={it.user}
          messageTime ={`${date.getHours()}:${date.getMinutes()}`}
          messageText ={it.text}
        />
      </React.Fragment>;
      }
    });
    return (
      <React.Fragment>
        <ul className="message-list">
        {massagesListItems}
      </ul>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.chat.messages,
    currentUserId: state.chat.chat.currentUserId
  }
};

const mapDispatchToProps = {

};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);