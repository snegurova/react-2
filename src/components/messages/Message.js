import React from "react";
import './Message.css'

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isLiked: false };
    this.onClick = this.onClick.bind(this);
  }
  onClick() {
    this.setState({
      isLiked: !this.state.isLiked
    });
  }
  render() {
    const likeButtonText = this.state.isLiked ? 'liked' : 'like';
    const likeButtonClass = this.state.isLiked ? 'message-liked' : 'message-like';
    return (
      <li className="message">
        <div
          className="message-user-avatar"
          style={{backgroundImage: `url("${this.props.avatarUrl}")`}}
        >
        </div>
        <div className="message-right">
          <div className="message-info">
            <span className="message-user-name">{this.props.userName} </span>
            <span>at </span>
            <span className="message-time">{this.props.messageTime}</span>
          </div>
          <div className="message-text">
            {this.props.messageText}
          </div>
        </div>
        <button className={likeButtonClass} onClick={ this.onClick }>{likeButtonText}</button>
      </li>
    );
  }
}

export default Message;